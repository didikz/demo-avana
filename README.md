# DEMO PURPOSE

## Installation

- Clone this repository
- Run `composer install`
- Setup environment with `cp .env.example .env`
- Generate application key `php artisan key:generate`
- Run `php artisan serve` then navigate to `http://localhost:8000` to make sure the app is running

## Rajaongkir Package

- Package GitHub URL: [Laravel Rajaongkir](https://github.com/didikz/laravel-rajaongkir)
- Package Packagist URL: [Laravel Rajaongkir](https://packagist.org/packages/didikz/laravel-rajaongkir)

To install and use the package, follow instruction below:

1. This application is already require the package, if you want to try in another application, install package with following command `composer require didikz/laravel-rajaongkir`
2. Setup `RAJAONGKIR_API_KEY` in `.env` file
3. Navigate to URL below in order to see the results:
   - Simulate fetch province data: `localhost:8000/api/provinces`
   - Simulate fetch province data by province id: `localhost:8000/api/provinces/{id}`
   - Simulate fetch cities data: `localhost:8000/api/provinces/{id}/cities`
   - Simulate fetch specific city: `localhost:8000/api/provinces/{id}/cities/{cityId}`
   - Simulate Cost Calculation: `localhost:8000/api/provinces/cost`
   
## Time Converter

Navigate to URL `localhost:8000/time/{hour}/{minute}` and adjust variable for `$hour` & `$minute` respectively 

or

run `php artisan test` to check class `TimeConverter` functionality. You may find the test class under `tests/unit/TimeConverterTest.php`

> for efficiency purpose, I used [Number to Words](https://github.com/kwn/number-to-words) package to convert numbers.

