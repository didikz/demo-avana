<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\TimeConverter;

class TimeConverterTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testTimeConverterIsValid()
    {
        $converter = new TimeConverter();
        $this->assertEquals("five o' clock", $converter->timeInWords('5', '00'));
        $this->assertEquals('one minute past five', $converter->timeInWords('5', '01'));
        $this->assertEquals('ten minutes past five', $converter->timeInWords('5', '10'));
        $this->assertEquals('quarter past five', $converter->timeInWords('5', '15'));
        $this->assertEquals('half past five', $converter->timeInWords('5', '30'));
        $this->assertEquals('twenty minutes to six', $converter->timeInWords('5', '40'));
        $this->assertEquals('quarter to six', $converter->timeInWords('5', '45'));

        // edge cases
        $this->assertEquals('quarter to one', $converter->timeInWords('12', '45'));
        $this->assertEquals('twenty minutes to one', $converter->timeInWords('12', '40'));
    }
}
