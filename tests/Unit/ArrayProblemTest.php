<?php

namespace Tests\Unit;

use App\ArrayProblem;
use PHPUnit\Framework\TestCase;

class ArrayProblemTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testArrayProblemSolved()
    {
        $arr = new ArrayProblem();
        $result = $arr->result(5, [[1,2,100], [2,5,100], [3,4,100]]);
        $this->assertEquals(100, $result);
    }
}
