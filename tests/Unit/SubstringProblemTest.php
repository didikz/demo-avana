<?php

namespace Tests\Unit;

use App\SubstringProblem;
use PHPUnit\Framework\TestCase;

class SubstringProblemTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testSubstringProblemIsSolved()
    {
        $substring = new SubstringProblem();
        $result = $substring->substringCount('aabaa', [[1,1], [1,4], [1,1], [1,4], [0,2]]);
        $this->assertEquals([1, 8, 1, 8, 5], $result);
    }
}
