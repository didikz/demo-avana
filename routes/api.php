<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Didikz\LaravelRajaongkir\Facade\Cost;
use Didikz\LaravelRajaongkir\Facade\Location;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('provinces', function () {
    return Location::province();
});

Route::get('provinces/{id}', function ($id) {
    return Location::province($id);
});

Route::get('provinces/{id}/cities', function ($id) {
    return Location::city($id);
});

Route::get('provinces/{id}/cities/{cityId}', function ($id, $cityId) {
    return Location::city($id, $cityId);
});

Route::get('cost', function (Request $request) {
    return Cost::destination(22)
                ->origin(55)
                ->weight(5000)
                ->courier('jne')
                ->calculate();
});
