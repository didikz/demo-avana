<?php

namespace App;

use NumberToWords\NumberToWords;

class TimeConverter
{
    /**
     * @param $h
     * @param $m
     * @return string
     */
    public function timeInWords($h, $m): string
    {
        $converter = (new NumberToWords())->getNumberTransformer('en');

        $minutes = (int) $m; // parse to integer
        $hours = (int) $h; // parse to integer
        $hourInWord = $converter->toWords($hours);
        $minutesInWord = $converter->toWords($minutes);

        if ($this->isHalf($minutes)) {
            return 'half past ' . $hourInWord;
        }

        if ($this->isMoreThanHalf($minutes)) {
            $minutesDiff = 60 - $minutes;
            if ($hours < 12) {
                $hours++;
            } else {
                $hours = 1;
            }
            if ($this->isQuarter($minutesDiff)) {
                return 'quarter to ' . $converter->toWords($hours);
            }
            return $converter->toWords($minutesDiff) . ' minutes to ' . $converter->toWords($hours);
        }

        if ($this->isBetweenHalfBefore($minutes)) {
            if ($this->isQuarter($minutes)) {
                return 'quarter past ' . $hourInWord;
            }
            return $minutesInWord . ' ' . $this->minutesDictionary($minutes) . ' past ' . $hourInWord;
        }

        return $hourInWord . " o' clock";
    }

    /**
     * @param int $minute
     * @return string
     */
    protected function minutesDictionary(int $minute)
    {
        return ($minute === 1) ? 'minute' : 'minutes';
    }

    /**
     * @param int $minute
     * @return bool
     */
    protected function isQuarter(int $minute)
    {
        return $minute % 15 === 0;
    }

    /**
     * @param int $minute
     * @return bool
     */
    protected function isHalf(int $minute)
    {
        return $minute === 30;
    }

    /**
     * @param $minute
     * @return bool
     */
    protected function isBetweenHalfBefore($minute)
    {
        return ($minute > 0 && $minute < 30);
    }

    /**
     * @param int $minute
     * @return bool
     */
    protected function isMoreThanHalf(int $minute)
    {
        return $minute > 30;
    }
}
