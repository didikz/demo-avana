<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimeConverter;

class TimeController extends Controller
{

    public function converter($h, $m, TimeConverter $timeConverter)
    {
        return $timeConverter->timeInWords($h, $m);
    }
}
