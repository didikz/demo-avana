<?php

namespace App;

class SubstringProblem
{
    public function substringCount(string $s, array $queries)
    {
        $result = [];
        foreach ($queries as $query) {
            $left = $query[0];
            $right = $query[1];
            $newString = substr($s, $left, $right);
            $stringLength = strlen($newString);
            if ($stringLength === 1) {
                $result[] = 1;
            }
        }
        return $result;
    }
}
